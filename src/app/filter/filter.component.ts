import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { Hero } from 'src/models/hero.model';
import { ApiService } from 'src/services/api.service';
import { FilterService } from 'src/services/filter.service';
import { FilterType } from '../enums';

@Component({
  selector: 'app-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.css']
})
export class FilterComponent implements OnInit, OnDestroy{
  subscriptions: Subscription = new Subscription();

  nameFilter: string = "";
  maxAgeFilter: string = "";
  minAgeFilter: string = "";
  sexFilter: string = "";

  sexList: string[] = [];
  heroes: Hero[] = [];

  constructor(
    private filterService: FilterService,
    private api: ApiService
    ) { }

  ngOnInit(): void {
    this.getSexList();
  }

  ngOnDestroy(): void {
    this.subscriptions.unsubscribe();
  }

  getSexList(){
    this.subscriptions.add(this.api.getHeroes().subscribe(_heroes => {
      this.heroes = _heroes;
      this.heroes.forEach(element => {
        this.sexList.push(element.sex);
        this.sexList = Array.from(new Set(this.sexList));
      });
    }));
  }

  filter(){
    let filters: {[type: number]: string} = {};
    filters[FilterType.NAME_LIKE] = this.nameFilter;
    filters[FilterType.MAX_AGE] = this.maxAgeFilter;
    filters[FilterType.MIN_AGE] = this.minAgeFilter;
    filters[FilterType.SEX] = this.sexFilter;
    this.filterService.broadcastEvent(filters);
  }

  resetFilters(){
    this.nameFilter = "";
    this.maxAgeFilter = "";
    this.minAgeFilter = "";
    this.sexFilter = "";
    this.getSexList();
  }
}
