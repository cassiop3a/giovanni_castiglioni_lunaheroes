import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { ListComponent } from './list/list.component';
import { FilterComponent } from './filter/filter.component'
import { DetailComponent } from './list/detail/detail.component';
import { FormsModule } from '@angular/forms';

import { MatTableModule } from '@angular/material/table';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { MatCardModule } from '@angular/material/card';
import { MatInputModule } from '@angular/material/input'
import { MatToolbarModule } from '@angular/material/toolbar'
import { MatSelectModule } from '@angular/material/select';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatDividerModule } from '@angular/material/divider'
import { MatIconModule } from '@angular/material/icon'

import { EditComponent } from './list/edit/edit.component';
import { DeleteComponent } from './list/delete/delete.component';
import { LoginComponent } from './login/login.component';
import { CreateComponent } from './list/create/create.component';

@NgModule({
  declarations: [
    AppComponent,
    ListComponent,
    FilterComponent,
    DetailComponent,
    EditComponent,
    DeleteComponent,
    LoginComponent,
    CreateComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatTableModule,
    MatButtonModule,
    MatDialogModule,
    MatCardModule,
    MatInputModule,
    FormsModule,
    MatToolbarModule,
    MatSelectModule,
    MatSidenavModule,
    MatDividerModule,
    MatIconModule
  ],
  exports: [
    BrowserAnimationsModule,
    MatTableModule,
    MatButtonModule,
    MatDialogModule,
    MatCardModule,
    MatInputModule,
    FormsModule,
    MatToolbarModule,
    MatSelectModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
