import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  username: string = "";
  password: string = "";

  onSubmit(){
    console.log(this.username);
    console.log(this.password);
    // Un servizio di autenticazione puo' essere aggiunto qui
  }

  resetForm(){
    this.username = "";
    this.password = "";
  }

  ngOnInit(): void {
  }

}
