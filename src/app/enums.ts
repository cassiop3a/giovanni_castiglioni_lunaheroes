export enum FilterType {
  NAME_LIKE,
  MAX_AGE,
  MIN_AGE,
  SEX
}
