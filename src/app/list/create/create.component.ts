import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { Hero } from 'src/models/hero.model';
import { ApiService } from 'src/services/api.service';
import { UUID } from 'angular2-uuid';
import { checkConditions } from '../condition.util';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class CreateComponent implements OnInit {
  name: string;
  age!: number;
  sex: string;
  particular_signs: string;
  notes: string;

  error: string = "Inserire i campi richiesti"
  showError: boolean = false;

  showConditionError: boolean = false;
  conditionError: string = "Uno o piu' campi non sono validi"

  success: string = "Eroe creato"
  showSuccess: boolean = false;

  subscriptions: Subscription = new Subscription();

  constructor(private api: ApiService) {
    this.name = "";
    this.sex = "";
    this.particular_signs = "";
    this.notes = "";
  }

  ngOnInit(): void {
  }

  createHero(){
    // Controllo delle condizioni per l'inserimento
    if(this.name && this.age && this.sex){
      this.showError = false;
      this.showConditionError = false;

      let hero: Hero = {
        // Normalmente in una API lo UUID viene generato lato server
        id: UUID.UUID(),
        name: this.name,
        age: this.age,
        sex: this.sex,
        particular_signs: this.particular_signs,
        notes: this.particular_signs
      }
      if(checkConditions(hero)){
        this.showSuccess = true;
        this.subscriptions.add(this.api.createHero(hero).subscribe());
      } else {
        this.showConditionError = true;
        this.showSuccess = false;
      }
    } else {
      this.showError = true;
      this.showSuccess = false;
    }
  }
}
