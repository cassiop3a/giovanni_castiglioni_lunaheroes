import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Subscription } from 'rxjs';
import { Hero } from 'src/models/hero.model';
import { ApiService } from 'src/services/api.service';

@Component({
  selector: 'app-delete',
  templateUrl: './delete.component.html',
  styleUrls: ['./delete.component.css']
})
export class DeleteComponent implements OnInit, OnDestroy {
  hero: Hero;
  subscriptions: Subscription = new Subscription();

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private api: ApiService
  ) {
    this.hero = data.hero;
  }

  ngOnDestroy(): void {
    this.subscriptions.unsubscribe();
  }

  deleteHero(){
    this.subscriptions.add(this.api.deleteHero(this.hero.id).subscribe());
  }

  ngOnInit(): void {
  }

}
