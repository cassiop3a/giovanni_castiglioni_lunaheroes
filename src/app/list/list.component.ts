import { Component, OnDestroy, OnInit } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { Subscription } from 'rxjs';
import { Hero } from 'src/models/hero.model';
import { ApiService } from 'src/services/api.service';
import { FilterService } from 'src/services/filter.service';
import { FilterType } from '../enums';
import { CreateComponent } from './create/create.component';
import { DeleteComponent } from './delete/delete.component';
import { DetailComponent } from './detail/detail.component';
import { EditComponent } from './edit/edit.component';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit, OnDestroy {
  heroes: Hero[] = [];
  subscriptions: Subscription = new Subscription();
  displayedColumns = ['UUID', 'name', "age", "sex", "particular_signs", "notes", "actions"];

  params: {[type: string]: string} = {};

  constructor(
    private api: ApiService,
    private filter: FilterService,
    private dialog: MatDialog,
    ) { }

  ngOnInit(): void {
    this.refreshList();
    this.subscribeToFilterService();
  }

  createDialog(){
    this.subscriptions.add(this.dialog.open(CreateComponent).afterClosed().subscribe(() => this.refreshList()));
  }

  detailDialog(hero: Hero) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.data = {
      hero: hero
     };
    this.subscriptions.add(this.dialog.open(DetailComponent, dialogConfig).afterClosed().subscribe(() => this.refreshList()));
  }

  editDialog(hero: Hero) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.data = {
      hero: hero
     };
    this.subscriptions.add(this.dialog.open(EditComponent, dialogConfig).afterClosed().subscribe(() => this.refreshList()));
  }

  deleteDialog(hero: Hero) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.data = {
      hero: hero
     };
    this.subscriptions.add(this.dialog.open(DeleteComponent, dialogConfig).afterClosed().subscribe(() => this.refreshList()));
  }

  refreshList(){
    this.subscriptions.add(this.api.getHeroes(this.params).subscribe(_heroes => this.heroes = _heroes));
  }

  subscribeToFilterService(): void {
    this.subscriptions.add(this.filter.onChange()
    .subscribe(_event => {
      this.params = {};
      if(_event[FilterType.NAME_LIKE]){
        this.params["name_like"] = _event[FilterType.NAME_LIKE];
      }
      if(_event[FilterType.MAX_AGE]){
        this.params["age_gte"] = _event[FilterType.MAX_AGE];
      }
      if(_event[FilterType.MIN_AGE]){
        this.params["age_lte"] = _event[FilterType.MIN_AGE];
      }
      if(_event[FilterType.SEX]){
        this.params["sex_like"] = _event[FilterType.SEX];
      }
      this.refreshList();
    }));
  }

  ngOnDestroy(): void {
    // Clean-up
    this.subscriptions.unsubscribe();
  }

}
