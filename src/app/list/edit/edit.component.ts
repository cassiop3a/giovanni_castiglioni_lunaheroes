import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Subscription } from 'rxjs';
import { Hero } from 'src/models/hero.model';

import { ApiService } from 'src/services/api.service';
import { checkConditions } from '../condition.util';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit, OnDestroy{
  hero: Hero;
  subscriptions: Subscription = new Subscription();

  saveMessage: string = "Dati salvati";
  showSaveMessage: boolean = false;

  errorMessage: string = "Dati invalidi";
  showErrorMessage: boolean = false;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private api: ApiService,
  ) {
    // Deep copy
    this.hero = JSON.parse(JSON.stringify(data.hero));
  }

  ngOnDestroy(): void {
    this.subscriptions.unsubscribe();
  }

  ngOnInit(): void {
  }

  saveDetails(){
    if(checkConditions(this.hero)){
      this.showSaveMessage = true;
      this.showErrorMessage = false;
      this.subscriptions.add(this.api.updateHero(this.hero.id, this.hero).subscribe());
    } else {
      this.showErrorMessage = true;
      this.showSaveMessage = false;
    }

  }
}
