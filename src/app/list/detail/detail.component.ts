import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Subscription } from 'rxjs';
import { Hero } from 'src/models/hero.model';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.css']
})
export class DetailComponent implements OnInit {
  hero: Hero;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
  ) {
    this.hero = data.hero;
  }

  ngOnInit(): void {
  }

}
