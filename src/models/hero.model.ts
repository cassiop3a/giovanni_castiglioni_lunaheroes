export interface Hero {
  id: string,
  name: string,
  age: number,
  sex: string,
  particular_signs?: string,
  notes?: string,
}
