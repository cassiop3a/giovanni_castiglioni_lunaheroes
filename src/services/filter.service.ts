import { EventEmitter, Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class FilterService {
  private filterEventEmitter: EventEmitter<{[type: string]: string}> = new EventEmitter();
  private filterObservable$: Observable<{[type: string]: string}> = this.filterEventEmitter.asObservable();

  constructor() { }

  broadcastEvent(filters: {[type: string]: string}){
    this.filterEventEmitter.emit(filters);
  }

  onChange(): Observable<{[type: string]: string}>{
    return this.filterObservable$;
  }
}
