import { HttpClient, HttpHeaders, HttpParams, HttpParamsOptions } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { FilterType } from 'src/app/enums';
import { Hero } from 'src/models/hero.model';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  private apiURL = "http://localhost:3000/heroes";
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  }

  constructor(private http: HttpClient) { }

  getHeroes(params?: {[type: string]: string}): Observable<Hero[]>{
    return this.http.get<Hero[]>(this.apiURL, {params: params});
  }

  createHero(hero:Hero): Observable <Hero>{
    return this.http.post<Hero>(this.apiURL, JSON.stringify(hero), this.httpOptions);
  }

  updateHero(id:string, hero:Hero): Observable <Hero>{
    return this.http.put<Hero>(this.apiURL + '/' + id, JSON.stringify(hero), this.httpOptions);
  }

  deleteHero(id:string): Observable <Hero>{
    return this.http.delete<Hero>(this.apiURL + '/' + id);
  }
}
