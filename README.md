# LunaHeroes
### Giovanni Castiglioni

## Per avviare l'applicativo
E' necessario installare i moduli necessari mediante il comando `npm install`.
Successivamente, bisogna aprire due terminali:
- il primo sara' il terminale per eseguire l'applicazione Angular con `ng serve`
- il secondo sara' per la nostra mock-API, si avvia con `npm run json:server`
